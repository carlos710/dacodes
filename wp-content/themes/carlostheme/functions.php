<?php
function carlos_shop_display_skus() {

    global $product;

    if ( $product->get_sku() ) {
        echo '<div class="product-meta">SKU: ' . $product->get_sku() . '</div>';
    }
}
add_action( 'woocommerce_after_shop_loop_item', 'carlos_shop_display_skus', 9 );