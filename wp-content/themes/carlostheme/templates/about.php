<?php
/* Template Name: About */
get_header();
?>
<main role="main" class="main-content-container about-page">
	<?php
	if ( have_posts() ) :
		while ( have_posts() ) :
			the_post();
            ?>
            <section class="about-section">
                <div class="container">
                    <?php
                    the_content();
                    ?>
                </div>
            </section>
        <?php endwhile; ?>
	<?php endif; ?>
</main>

<?php get_footer(); ?>
